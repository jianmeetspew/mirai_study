#!/usr/bin/env python

import glob
import pickle
from pathlib import Path
from typing import Union, Dict, List

import fire
import numpy as np
from elftools.elf.elffile import ELFFile, ELFParseError
from hmmlearn import hmm


ARCHITECTURES = {
    "EM_386": "intel",
    "EM_X86_64": "intel",
    "EM_ARM": "arm",
    "EM_AARCH64": "arm",
    "EM_MIPS": "mips",
    "EM_PPC": "ppc",
}
MNENOMICS: Dict[str, int] = dict()


def build_mnenomics_buf(raw_buf) -> List[str]:
    filtered_comments_buf = filter(
        lambda x: x != "",
        map(
            lambda x: x.strip() if ";" not in x else "".join(x.split(";")[:1]).strip(),
            raw_buf,
        ),
    )
    assembly_buf = map(
        lambda line: line.split("\t")[1],
        filter(lambda x: "\t" in x, filtered_comments_buf),
    )
    mnenomics_buf = map(lambda line: line.split(" ")[0], assembly_buf)

    return list(mnenomics_buf)


def get_architecture_ext(binary_parent: Path) -> str:
    architectures = set()
    for name in glob.glob(str(binary_parent) + "/**/*.elf", recursive=True):
        try:
            with open(name, "rb") as f:
                elf = ELFFile(f)
                header = elf.header
                architectures.add(ARCHITECTURES[header.e_machine])
        except (TypeError, ELFParseError):
            print(f"error in {name}")
            continue

        if len(architectures) != 1:
            raise IOError("more than one type of architecture")
        return architectures.pop()

    raise IOError("parent directory has no ELF files")


def build_mnenomics_index(binary_parent: Path):
    global MNENOMICS

    extension = get_architecture_ext(binary_parent)
    index_path = Path(f"index.{extension}")
    if index_path.exists():
        with open(f"index.{extension}", "r") as f:
            mnenomics = f.readlines()

        mnenomics = [x.strip() for x in mnenomics]
        for i, m in enumerate(mnenomics):
            MNENOMICS[m] = i


def cli_main(binary_parent: Path):
    build_mnenomics_index(binary_parent)

    X_sequence = list()
    lengths = list()

    for name in glob.glob(str(binary_parent) + "/**/*.elf", recursive=True):
        int_sequence_path = Path(f"{name}.dsm.int")
        if not int_sequence_path.exists():
            dsm_path = Path(f"{name}.dsm")
            if not dsm_path.exists():
                raise FileNotFoundError("[!] dsm file does not exist!")

            with open(str(dsm_path), "r") as f:
                buf = f.readlines()

            mnenomics = build_mnenomics_buf(buf)

            with open(f"{name}.dsm.int", "w") as f:
                for m in mnenomics:
                    f.write(f"{MNENOMICS[m]}\n")

        with open(str(int_sequence_path), "r") as f:
            buf = f.readlines()

        sequence = [int(x.strip()) for x in buf]
        X_sequence.append(np.array(sequence))
        lengths.append(len(sequence))

    X = np.concatenate(X_sequence).reshape((-1, 1))

    for i in range(2, 7):
        print(f"[+] training N={i} on {len(lengths)} binary samples")
        model = hmm.CategoricalHMM(n_components=i, n_iter=100).fit(X, lengths)
        with open(f"vanilla_hmm_{i}.pickle", "wb") as f:
            pickle.dump(model, f)


if __name__ == "__main__":
    fire.Fire(cli_main)
