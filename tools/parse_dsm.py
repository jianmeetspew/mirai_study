#!/usr/bin/env python

import os
import pickle
from collections import Counter
from functools import reduce
from pathlib import Path
from typing import List, NamedTuple, Union

import fire
from elftools.elf.elffile import ELFFile, ELFParseError


class Binary(NamedTuple):
    basename: str
    machine: str
    sequence: List[Union[str, int]]


def build_mnenomics_buf(raw_buf) -> List[str]:
    filtered_comments_buf = filter(
        lambda x: x != "",
        map(
            lambda x: x.strip() if ";" not in x else "".join(x.split(";")[:1]).strip(),
            raw_buf,
        ),
    )
    assembly_buf = map(
        lambda line: line.split("\t")[1],
        filter(lambda x: "\t" in x, filtered_comments_buf),
    )
    mnenomics_buf = map(lambda line: line.split(" ")[0], assembly_buf)

    return list(mnenomics_buf)


def get_mnenomics_from_basedir(p: Path) -> Binary:
    files = os.listdir(p)

    dsm_file = next(filter(lambda x: "dsm" in x, files), None)
    elf_file = next(filter(lambda x: "elf." not in x, files), None)

    if dsm_file is not None and elf_file is not None:
        with open(str(p / dsm_file)) as f:
            buf = f.readlines()

        mnenomics = build_mnenomics_buf(buf)

        with open(
            str(p / elf_file),
            "rb",
        ) as f:
            elf = ELFFile(f)
            header = elf.header

        return Binary(
            basename=dsm_file.split(".")[0],
            machine=header.e_machine,
            sequence=mnenomics,
        )
    else:
        print(f"[!] either dsm or elf file not found within path file {str(p)}")
        raise IOError


def run_on(p: str):
    target_path = Path(p)
    binaries = list()
    error_count = 0

    basedirs = os.listdir(p)
    for directory in basedirs:
        try:
            binaries.append(get_mnenomics_from_basedir(target_path / directory))
        except IOError:
            error_count += 1
        except (ELFParseError, TypeError) as e:
            print(f"[!] Parsing error for {str(target_path / directory)}: {e}")
            error_count += 1

    available_archs = set(map(lambda x: x.machine, binaries))
    print(f"[!] amount of samples that did not parse: {error_count}")

    import pdb

    pdb.set_trace()

    print(f"[+] building mnenomic index")
    mnenomic_index_items = list(
        set(reduce(lambda x, y: x + y, map(lambda x: x.sequence, binaries)))
    )
    mnenomic_index = dict()
    for i, item in enumerate(mnenomic_index_items):
        mnenomic_index[item] = i

    architecture_stats = Counter(map(lambda x: x.machine, binaries))
    print(f"[+] rebuilding sequence")
    for i, bin in enumerate(binaries):
        binaries[i] = (
            bin.basename,
            bin.machine,
            list(
                map(
                    lambda x: mnenomic_index[x],
                    bin.sequence,
                )
            ),
        )

    print(f"[+] available architectures: {architecture_stats}")

    with open("mirai.pk1", "wb") as f:
        pickle.dump([architecture_stats, mnenomic_index, binaries], f)


if __name__ == "__main__":
    fire.Fire(run_on)
