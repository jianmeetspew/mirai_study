use std::ffi::OsStr;
use std::fs;
use std::process::Command as CommandRunner;
use std::{io, path::Path};

use clap::{arg, Command};
use elf::endian::AnyEndian;
use elf::ElfBytes;

fn convert_osstr_to_string(p: &OsStr) -> String {
    p.to_string_lossy().into_owned()
}

fn retdec_binary(base_dir: &Path) -> Result<(), io::Error> {
    for r in fs::read_dir(base_dir)? {
        if let Ok(parent_dir) = r {
            if parent_dir.path().is_dir() {
                for entry in fs::read_dir(parent_dir.path())? {
                    if let Ok(target) = entry {
                        if convert_osstr_to_string(target.path().to_owned().extension().unwrap())
                            == "elf"
                        {
                            println!("[+] running retdec-decompiler on {:?}", target.path());
                            let mut retdec_process = CommandRunner::new("retdec-decompiler")
                                .current_dir(parent_dir.path())
                                .arg(target.file_name())
                                .spawn()?;
                            let status = retdec_process.wait()?;
                            if !status.success() {
                                eprintln!(
                                    "[!] retdec-decompiler unable to decompile {:?}",
                                    target.file_name()
                                );
                            }
                        }
                    }
                }
            }
        }
    }
    Ok(())
}

fn has_retdec_artifact(target_dir: &Path) -> bool {
    for e in fs::read_dir(target_dir).expect("[!] could not read directory") {
        if let Ok(entry) = e {
            if convert_osstr_to_string(entry.path().to_owned().extension().unwrap()) == "dsm" {
                return true;
            }
        }
    }
    false
}

fn main() {
    let matches = Command::new("hmm_corpus_add")
        .version("1.0")
        .author("Raymond S. <rsou.txrx@gmail.com>")
        .arg(arg!(-t --target <VALUE>).required(true))
        .arg(arg!(-p --pickle <VALUE>).default_value("corpus.pickle"))
        .get_matches();

    let target_file = Path::new(matches.get_one::<String>("target").expect("required"));
    if !target_file.exists() || target_file.is_dir() {
        panic!("[!] invalid target file")
    }

    let file_data = fs::read(target_file).expect("[!] could not read target file");
    let slice = file_data.as_slice();
    let elf_file =
        ElfBytes::<AnyEndian>::minimal_parse(slice).expect("[!] could not parse target as ELF");

    let elf_abs_file = target_file
        .canonicalize()
        .expect("[!] failed to canonicalize");
    println!(
        "[+] {}: {}",
        elf_abs_file.display(),
        elf::to_str::e_machine_to_human_str(elf_file.ehdr.e_machine).unwrap()
    );

    let parent = target_file
        .parent()
        .expect("[!] failed to get parent directory");
    if !has_retdec_artifact(parent) {
        let _ = retdec_binary(&parent);
    } else {
        println!("[*] target has already been decompiled");
    }

    let dsm_file_name = format!(
        "{}.dsm",
        elf_abs_file.to_str().expect("[!] convert to string")
    );
    let dsm_path = Path::new(&dsm_file_name);
    if !dsm_path.exists() {
        panic!("[!] dsm file does not exist after decompilation")
    }
    println!("dsm path: {}", dsm_path.to_string_lossy());
}
