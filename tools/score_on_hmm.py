#!/usr/bin/env python

import glob
import pickle
import sys
from pathlib import Path
from typing import Dict, List, cast

import fire
import numpy as np
from elftools.elf.elffile import ELFFile, ELFParseError
from hmmlearn import hmm


ARCHITECTURES = {
    "EM_386": "intel",
    "EM_X86_64": "intel",
    "EM_ARM": "arm",
    "EM_AARCH64": "arm",
    "EM_MIPS": "mips",
    "EM_PPC": "ppc",
}
MNENOMICS: Dict[str, int] = dict()


def build_mnenomics_buf(raw_buf) -> List[str]:
    filtered_comments_buf = filter(
        lambda x: x != "",
        map(
            lambda x: x.strip() if ";" not in x else "".join(x.split(";")[:1]).strip(),
            raw_buf,
        ),
    )
    assembly_buf = map(
        lambda line: line.split("\t")[1],
        filter(lambda x: "\t" in x, filtered_comments_buf),
    )
    mnenomics_buf = map(lambda line: line.split(" ")[0], assembly_buf)

    return list(mnenomics_buf)


def get_architecture_ext(binary_parent: Path) -> str:
    architectures = set()
    for name in glob.glob(str(binary_parent) + "/**/*.elf", recursive=True):
        try:
            with open(name, "rb") as f:
                elf = ELFFile(f)
                header = elf.header
                architectures.add(ARCHITECTURES[header.e_machine])
        except (TypeError, ELFParseError):
            print(f"error in {name}")
            continue

        if len(architectures) != 1:
            raise IOError("more than one type of architecture")
        return architectures.pop()

    raise IOError("parent directory has no ELF files")


def build_mnenomics_index(binary_parent: Path):
    global MNENOMICS

    extension = get_architecture_ext(binary_parent)
    index_path = Path(f"index.{extension}")
    if index_path.exists():
        with open(f"index.{extension}", "r") as f:
            mnenomics = f.readlines()

        mnenomics = [x.strip() for x in mnenomics]
        for i, m in enumerate(mnenomics):
            MNENOMICS[m] = i


def cli_main(binary_parent: Path, model_path: Path, limit: int = sys.maxsize):
    with open(str(model_path), "rb") as f:
        model = pickle.load(f)
        model = cast(hmm.CategoricalHMM, model)

    build_mnenomics_index(binary_parent)

    X_sequence = list()
    lengths = list()
    item_num = 0

    for name in glob.glob(str(binary_parent) + "/**/*.elf", recursive=True):
        int_sequence_path = Path(f"{name}.dsm.int")
        if not int_sequence_path.exists():
            dsm_path = Path(f"{name}.dsm")
            if not dsm_path.exists():
                continue

            with open(str(dsm_path), "r") as f:
                buf = f.readlines()

            mnenomics = build_mnenomics_buf(buf)

            with open(f"{name}.dsm.int", "w") as f:
                for m in mnenomics:
                    try:
                        f.write(f"{MNENOMICS[m]}\n")
                    except KeyError:
                        pass

        with open(str(int_sequence_path), "r") as f:
            buf = f.readlines()

        sequence = [int(x.strip()) for x in buf]
        sequence = list(filter(lambda x: x < len(MNENOMICS), sequence))
        if len(sequence) > 0:
            X_sequence.append(np.array(sequence))
            lengths.append(len(sequence))

            item_num += 1
            if item_num >= limit:
                break

    result = list()

    for X, size in zip(X_sequence, lengths):
        score = model.score(X.reshape((-1, 1))) / size
        result.append(score)

    print(result)


if __name__ == "__main__":
    fire.Fire(cli_main)
