use elf::endian::AnyEndian;
use elf::ElfBytes;
use std::collections::HashMap;
use std::fs;
use std::io;

fn main() -> Result<(), io::Error> {
    let mut architectures: HashMap<String, u32> = HashMap::new();
    if let Ok(entries_res) = fs::read_dir("/Users/monday/workspace/mirai_study/samples/generated") {
        for sample_dirs in entries_res.filter_map(Result::ok) {
            if let Ok(dir_reader) = fs::read_dir(sample_dirs.path()) {
                for file in dir_reader.filter_map(Result::ok) {
                    if file.path().to_owned().extension().unwrap() == "elf" {
                        let file_data = fs::read(file.path())?;
                        let slice = file_data.as_slice();
                        if let Ok(elf_file) = ElfBytes::<AnyEndian>::minimal_parse(slice) {
                            // let e_mach =
                            //     elf::to_str::e_machine_to_human_str(elf_file.ehdr.e_machine)
                            //         .unwrap()
                            //         .to_owned();
                            let e_mach = elf::to_str::e_machine_to_string(elf_file.ehdr.e_machine);
                            if !architectures.contains_key(&e_mach) {
                                architectures.insert(e_mach.clone(), 0);
                            } else {
                                architectures.insert(
                                    e_mach.clone(),
                                    architectures.get(&e_mach).unwrap() + 1,
                                );
                            }

                            if e_mach == "EM_X86_64" {
                                let filename = sample_dirs.file_name();
                                let destination =
                                    String::from("/Users/monday/workspace/mirai_study/intel/x64/")
                                        + &filename.to_string_lossy();
                                // println!("moving from {:?} to {:?}", sample_dirs, destination);
                                fs::rename(sample_dirs.path(), destination);
                            } else if e_mach == "EM_386" {
                                let filename = sample_dirs.file_name().to_owned();
                                let destination =
                                    String::from("/Users/monday/workspace/mirai_study/intel/x86/")
                                        + &filename.to_string_lossy();
                                fs::rename(sample_dirs.path(), destination);
                            }
                        }
                    }
                }
            }
        }
    }

    println!("{:?}", architectures);

    Ok(())
}
