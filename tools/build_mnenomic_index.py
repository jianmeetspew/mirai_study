#!/usr/bin/env python

import glob
from pathlib import Path
from typing import Dict, List, Union

import fire
from elftools.elf.elffile import ELFFile, ELFParseError


ARCHITECTURES = {
    "EM_386": "intel",
    "EM_X86_64": "intel",
    "EM_ARM": "arm",
    "EM_AARCH64": "arm",
    "EM_MIPS": "mips",
    "EM_PPC": "ppc",
}
MNENOMICS: Dict[Union[str, int], Union[str, int]] = dict()


def build_mnenomics_buf(raw_buf) -> List[str]:
    filtered_comments_buf = filter(
        lambda x: x != "",
        map(
            lambda x: x.strip() if ";" not in x else "".join(x.split(";")[:1]).strip(),
            raw_buf,
        ),
    )
    assembly_buf = map(
        lambda line: line.split("\t")[1],
        filter(lambda x: "\t" in x, filtered_comments_buf),
    )
    mnenomics_buf = map(lambda line: line.split(" ")[0], assembly_buf)

    return list(mnenomics_buf)


def get_architecture_ext(binary_parent: Path) -> str:
    architectures = set()
    for name in glob.glob(str(binary_parent) + "/**/*.elf", recursive=True):
        try:
            with open(name, "rb") as f:
                elf = ELFFile(f)
                header = elf.header
                architectures.add(ARCHITECTURES[header.e_machine])
        except (TypeError, ELFParseError):
            print(f"error in {name}")
            continue

        if len(architectures) != 1:
            raise IOError("more than one type of architecture")
        return architectures.pop()

    raise IOError("parent directory has no ELF files")


def build_mnenomics_index(extension: str):
    global MNENOMICS

    index_path = Path(f"index.{extension}")
    if index_path.exists():
        with open(f"index.{extension}", "r") as f:
            mnenomics = f.readlines()

        mnenomics = [x.strip() for x in mnenomics]
        for i, m in enumerate(mnenomics):
            MNENOMICS[m] = i
            MNENOMICS[i] = m


def save_mnenomics_index(extension: str):
    global MNENOMICS

    index_path = Path(f"index.{extension}")
    with open(f"index.{extension}", "w") as f:
        for i in range(len(MNENOMICS) // 2):
            f.write(f"{MNENOMICS[i]}\n")


def cli_main(binary_parent: Path):
    global MNENOMICS

    extension = get_architecture_ext(binary_parent)
    build_mnenomics_index(extension)
    next_index = len(MNENOMICS) // 2

    for name in glob.glob(str(binary_parent) + "/**/*.elf", recursive=True):
        print(f"[+] opening {name}.dsm")
        try:
            with open(f"{name}.dsm", "r") as f:
                buf = f.readlines()

            mnenomics = build_mnenomics_buf(buf)

            with open(f"{name}.dsm.int", "w") as f:
                for m in mnenomics:
                    if m not in MNENOMICS:
                        MNENOMICS[next_index] = m
                        MNENOMICS[m] = next_index
                        next_index += 1
                    f.write(f"{MNENOMICS[m]}\n")
        except FileNotFoundError:
            print(f"[!] file: {name}.dsm does not exist!")

    save_mnenomics_index(extension)


if __name__ == "__main__":
    fire.Fire(cli_main)
