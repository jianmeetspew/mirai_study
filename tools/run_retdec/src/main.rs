use std::ffi::OsStr;
use std::fs;
use std::process::Command as CommandRunner;
use std::{io, path::Path};

use clap::{arg, Arg, ArgAction, Command};
use glob::glob;

#[derive(Debug)]
enum Error {
    Io(io::Error),
    Glob(glob::GlobError),
}

impl From<io::Error> for Error {
    fn from(other: io::Error) -> Error {
        Error::Io(other)
    }
}

impl From<glob::GlobError> for Error {
    fn from(other: glob::GlobError) -> Error {
        Error::Glob(other)
    }
}

fn convert_osstr_to_string(p: &OsStr) -> String {
    p.to_string_lossy().into_owned()
}

fn unzip_files(
    source_dir: &Path,
    destination_dir: &Path,
    password: Option<&String>,
) -> Result<(), io::Error> {
    if !destination_dir.exists() {
        fs::create_dir_all(destination_dir)?;
    }

    for entry in fs::read_dir(source_dir)? {
        let file_path = entry?.path();

        if let Some(extension) = file_path.extension() {
            let hash_string = convert_osstr_to_string(&(file_path.file_stem().unwrap()));
            if extension == "zip" {
                println!("[+] unzipping {:?}", file_path);
                let extraction_dir = format!(
                    "-o{}/mirai_{}",
                    destination_dir.to_str().unwrap(),
                    hash_string[..hash_string.len() - 53].to_string()
                );
                let mut unzip_process = CommandRunner::new("7za")
                    .arg("x")
                    .arg(file_path.to_str().unwrap())
                    .arg(format!("-p{}", password.map_or("", |p| p)))
                    .arg("-bso0") // silence output
                    .arg("-aoa") // overwrite mode
                    .arg(&extraction_dir)
                    .spawn()?;

                let status = unzip_process.wait()?;
                if !status.success() {
                    return Err(io::Error::new(io::ErrorKind::Other, "Unzip process failed"));
                }
            }
        }
    }

    Ok(())
}

fn retdec_binaries(base_dir: &Path) -> Result<(), Error> {
    let pattern = format!("{}/**/*.elf", base_dir.to_str().unwrap());
    let mut binaries = glob(&pattern).expect("Failed to read glob pattern");
    binaries.try_for_each(|entry| -> io::Result<()> {
        match entry {
            Ok(bin) => {
                println!("[+] running retdec-decompiler on {:?}", bin.as_path());
                let mut retdec_process = CommandRunner::new("retdec-decompiler")
                    .current_dir(bin.parent().unwrap())
                    .arg(bin.file_name().unwrap())
                    .spawn()?;
                let status = retdec_process.wait()?;
                if !status.success() {
                    eprintln!(
                        "[!] retdec-ecompiler unable to decompile {:?}",
                        bin.file_name()
                    );
                }
            }
            Err(e) => eprintln!("Error: {:?}", e),
        }
        Ok(())
    })?;

    Ok(())
}

fn main() -> Result<(), Error> {
    let matches = Command::new("run_retdec")
        .version("1.0")
        .author("Raymond S. <rsou.txrx@gmail.com>")
        .arg(arg!(-s --source <VALUE>).required(false))
        .arg(
            arg!(-d --destination <VALUE> "Root directory where retdec will glob into")
                .required(true),
        )
        .arg(arg!(-p --password <VALUE>).required(false))
        // .arg(arg!(-z --unzip ... "turns on unzipping from source to destination"))
        .arg(
            Arg::new("unzip")
                // .short("z")
                .long("unzip")
                .action(ArgAction::SetTrue)
                .help("turns on unzipping from source to destination"),
        )
        .get_matches();

    let unzip = matches.get_flag("unzip");
    let destination_dir = Path::new(matches.get_one::<String>("destination").expect("required"));
    if unzip {
        let source_dir = Path::new(
            matches
                .get_one::<String>("source")
                .expect("required if we are not unzipping"),
        );
        println!(
            "source dir {:?}, exists: {:?}",
            source_dir,
            source_dir.exists()
        );

        let _ = unzip_files(
            &source_dir,
            &destination_dir,
            matches.get_one::<String>("password"),
        );
    }

    let _ = retdec_binaries(&destination_dir);

    Ok(())
}
