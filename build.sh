#!/usr/bin/env bash

LLVM_DIR=$HOME/workspace/llvm/llvm-project/build
cmake -DLT_LLVM_INSTALL_DIR=$LLVM_DIR -Bbuild -S. -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -G Ninja
bear -- cmake --build build
